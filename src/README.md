# SRC folder is where theme development happens.

## Tools

### NPM

We use [npm] insead of [yarn] because most of the reasons why *yarn* was created
are now covered by *npm*. Also, *yarn* has proven to be a bit unreliable with
its servers.

### Webpack

Because *Webpack* is magical and makes development so much easier.

### Bulma

This theme uses [Bulma], a modern open source CSS framework based on Flexbox.
The size of *Bulma* when *gzipped* is `~21Kb`.

[npm]: https://www.npmjs.com
[yarn]: https://yarnpkg.com
[Bulma]: https://bulma.io
