import gulp from 'gulp';

import autoprefixer from 'autoprefixer';
import log from 'fancy-log';
import plumber from 'gulp-plumber';
import pug from 'gulp-pug';
import sass from 'gulp-sass';
import maps from 'gulp-sourcemaps';

import webpack from 'webpack';
import webpackConfig from './src/webpack.conf';

gulp.task('pug', () =>
          gulp.src(['pug/**/*.pug'])
          .pipe(plumber())
          .pipe(pug())
          .pipe(gulp.dest('../layouts')));

gulp.task('sass', () =>
          gulp.src(['src/sass/**/*.scss'])
          .pipe(plumber())
          .pipe(maps.init())
          .pipe(sass({outputStyle: 'compressed'}))
          .pipe(postcss([
            autoprefixer({
              browsers: [">0.1%"],
              cascade: false,
              remove: true
            })
          ]))
          .pipe(gulp.dest('../static/dist')));

gulp.task('js', (db) => {
  const myConfig = Object.assign({}, webpackConfig);
  webpack(myConfig, (err, stats) => {
    if (err) {
      throw new gutil.PluginError('webpack', err);
    }
    log('[webpack]', stats.toString({
      colors: true,
      progress: true
    }));
    cb();
  });
});

gulp.task('watch', () => {
  gulp.watch('pug/**/*.pug', ['pug']);
  gulp.watch('sass/**/*.scss', ['sass']);
  gulp.watch('js/**/*.js', ['js']);
});

gulp.task('default', ['pug', 'sass', 'js']);
